

        //Replace Anything that's not a number
        $('.numbersOnly').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
        $('.numbersOnly').keydown(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });

        //Form Input Formatting
        $('#phoneNum').keyup(function () {
            $(this).val($(this).val().replace(/(\d{3})\)?(\d{3})\-?(\d{4})/, '($1) $2-$3'))
        });
        $('#phoneNum').keydown(function () {
            $(this).val($(this).val().replace(/(\d{3})\)?(\d{3})\-?(\d{4})/, '($1) $2-$3'))
        });
        $('#cCard').keyup(function () {
            $(this).val($(this).val().replace(/(\d{4})\)?(\d{4})\-?(\d{4})\-?(\d{4})/, '$1 $2 $3 $4'))
        });
        $('#cCard').keydown(function () {
            $(this).val($(this).val().replace(/(\d{4})\)?(\d{4})\-?(\d{4})\-?(\d{4})/, '$1 $2 $3 $4'))

        });

        $("#myForm").validate({
            rules: {
                creditCard: {
                    required: true,
                },
                zipcode: {
                    required: true,
                    minlength: 5,
                    maxlength: 5
                },
                phoneNum: {
                    required: true,
                }
            },
            messages: {

                creditCard: {
                    required: "Enter your Credit Card",
                    minlength: "Please enter a valid Credit Card",

                },
                zipcode: {
                    required: "Enter your Zipcode",
                    minlength: "Please enter a valid Zipcode",
                },

                phoneNum: {
                    required: "Enter your phone number",
                    minlength: "Please enter a valid phone number",
                }
            }
        });