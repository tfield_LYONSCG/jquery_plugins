 $.fn.accordFolder = function (x, button, allButtons) {
            let buttonName = $(button).attr('id');
            if (allButtons == true) {
                if (buttonName == "open") {
                    $(x).slideDown();
                }
                if (buttonName == "close") {
                    $(x).slideUp();
                }
                $(button).css("background-color", "lightblue");
            } else {
                let newName = x.attr('id')
                let newNameNumber = +newName.substring(7);
                $(button).text(newName);
                $(x).fadeToggle();
                this.css("background-color", "lightblue");
            }
        };

        $(".slideAll").on('click', function (el) {
            let header = $('.jumbotron');
            header.accordFolder(header, this, true);
        });
        $(".slide").on('click', function () {
            let folder = $(this).next();
            folder.accordFolder(folder, this);
        });