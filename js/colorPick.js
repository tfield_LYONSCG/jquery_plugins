   $.fn.showMouse = function () {
            this.css("color", assignedColor);
        };
        $.fn.showoutMouse = function () {
            this.css("color", "black");
        };
        

        //Highlight stuff as you go by it but only specific tags
        $(".jumbotron").on('mouseover', function () {
            $(this).showMouse();
        });
        $(".jumbotron").on('mouseout', function () {
            $(this).showoutMouse();
        });
        //ColorPicker Buttons
        $(".colorPick").on('click', function () {
            let colorId = $(this).attr("id");
            if (colorId != "random") {
                assignedColor = colorId;
                $("#rColorP").text(colorId);
                $("#rColorP").css("color", colorId);
            } else {
                //Generates the Random Hexidecimal 
                assignedColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
                $("#rColorP").text(assignedColor.toUpperCase());
                $('#random').css("background",assignedColor);
                $("#rColorP").css("color", assignedColor);
            }
         });